Crypto Currency Auto Trader
==

Important Notes
--

This project is completed as a way to learn about crypto currency trading as well as neural networks. I have made this public so that
if other people wish to use it they can however I cannot verify or prove that the system will function as inteneded.

People involved in the project
--

| Name | Role |
| --- | --- |
| Curtis Andersen | programming |
| Mark Rojas | Tax adviser |


System Functionality
--

The way the system works is through loading the historic crypto to us comparison values of given coins. Then uses a *modified NEAT neural network* it locates the points at which trades are considered more optimal. The trades are conducted currently only over [CEX.io](https://cex.io/?utm_source=google&utm_medium=cpc&utm_term=60170301534&utm_campaign=1616839813&utm_content=307875042180). 

Common Trading Coins
--

*These are the common coins used by the system*

`[BTC, LTC, DOGE, XRP]`

there are many more that can be used however adding a new coin does not work well with the current system of the network algrithum. Thus it is good to limit
any trading attempts to a max of 6 coins.


System Controls
--

Currently, there is no GUI for the user however there is a console system that can be used.

Current Commands

* log `log name`
  * --get `number of lines` : gets the number of lines from a given log
  * --size `new number of lines to maintain` : sets the length of the log to hold this will decrease storage usage the shorter
      the length is.
* exit : exits the system.
