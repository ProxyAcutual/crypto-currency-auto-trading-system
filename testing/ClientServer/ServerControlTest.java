package ClientServer;

import ClientServer.ClientSocket;
import ClientServer.ServerControl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ServerControlTest {

    @Test
    @DisplayName("Test Basic Connection")
    public void basicConnection() throws InterruptedException {
        ServerControl server = new ServerControl(3333);
        Thread.sleep(1000);
        ClientSocket client1 = new ClientSocket("localhost", 3333);
        Thread.sleep(500);
        assert(server.getConnections().length == 1);
        server.killServer();
        Thread.sleep(500);
    }

    @Test
    @DisplayName("Test two Connections")
    public void twoConnections() throws InterruptedException {
        ServerControl server = new ServerControl(3333);
        Thread.sleep(1000);
        ClientSocket client1 = new ClientSocket("localhost", 3333);
        ClientSocket client2 = new ClientSocket("localhost", 3333);
        ClientSocket client3 = new ClientSocket("localhost", 3333);
        Thread.sleep(1000);
        assert(server.getConnections().length == 3);
        server.killServer();
        Thread.sleep(500);
    }

    @Test
    @DisplayName("Basic Data Transfer")
    public void baseDataTransfer() throws InterruptedException {
        ServerControl server = new ServerControl(3333);
        Thread.sleep(1000);
        ClientSocket client1 = new ClientSocket("localhost", 3333);
        client1.sendData("test1");
        ClientSocket clientServer = server.getConnections()[0];
        Thread.sleep(1000);

        assert(clientServer.getNewInputs().length == 1);

        client1.sendData("test2");
        Thread.sleep(500);
        assert(clientServer.getNewInputs()[0].matches("test2"));

        clientServer.sendData("test3");
        Thread.sleep(300);
        assert(client1.getNewInputs()[0].matches("test3"));

        server.killServer();
        Thread.sleep(500);
    }

    @Test
    @DisplayName("Client Closure With Server Shutdown")
    public void ClientClosure() throws InterruptedException {
        ServerControl server = new ServerControl(3333);
        Thread.sleep(1000);
        ClientSocket client1 = new ClientSocket("localhost", 3333);
        Thread.sleep(500);
        server.killServer();
        Thread.sleep(1000);
        client1.sendData("test1");
        Thread.sleep(500);
        assert(server.getConnections().length == 0);
        //client1.killSocket();
        assert(client1.isConnected() == false);
    }

    @Test
    @DisplayName("Testing Removal Of Dead Connections")
    public void ClientConnectionClosed() throws InterruptedException {
        ServerControl server = new ServerControl(3333);
        Thread.sleep(1000);
        ClientSocket client1 = new ClientSocket("localhost", 3333);
        Thread.sleep(500);
        client1.killSocket();
        Thread.sleep(500);
        assert(server.getConnections().length == 0);
        assert(client1.isConnected() == false);
        server.killServer();
    }

    @Test
    @DisplayName("Server client Communications heavy transfer")
    public void ClientServerHeavy() throws InterruptedException {
        ServerControl server = new ServerControl(3333);
        Thread.sleep(1000);
        ClientSocket client1 = new ClientSocket("localhost", 3333);
        Thread.sleep(500);
        client1.sendData("test0");
        client1.sendData("test1");
        server.getConnections()[0].sendData("server0");
        client1.sendData("test2");
        server.getConnections()[0].sendData("server1");
        client1.sendData("test3");
        server.getConnections()[0].sendData("server2");
        server.getConnections()[0].sendData("server3");

        Thread.sleep(1000);
        String[] returns = client1.getNewInputs();
        for(int i = 0; i < returns.length; i++){
            assert(returns[i].matches("server" + i));
        }

        returns = server.getConnections()[0].getNewInputs();
        for(int i = 0; i < returns.length; i++){
            assert(returns[i].matches("test" + i));
        }

        server.killServer();
        Thread.sleep(1000);
        assert(!client1.isConnected());
    }

    @Test
    @DisplayName("connection removed from connected sessions")
    public void removeClients() throws InterruptedException {
        ServerControl server = new ServerControl(3333);
        Thread.sleep(1000);
        ClientSocket client1 = new ClientSocket("localhost", 3333);
        ClientSocket client2 = new ClientSocket("localhost", 3333);
        ClientSocket client3 = new ClientSocket("localhost", 3333);
        Thread.sleep(1000);
        assert(server.getConnections().length == 3);
        client1.killSocket();
        Thread.sleep(2000);
        assert(server.getConnections().length == 2);
        client2.killSocket();
        Thread.sleep(2000);
        assert(server.getConnections().length == 1);
        client3.killSocket();
        Thread.sleep(2000);
        assert(server.getConnections().length == 0);
        server.killServer();
    }

    @Test
    @DisplayName("Massive Client ShutDown")
    public void removeServerFromClients() throws InterruptedException {
        ServerControl server = new ServerControl(3333);
        Thread.sleep(1000);
        ClientSocket client1 = new ClientSocket("localhost", 3333);
        ClientSocket client2 = new ClientSocket("localhost", 3333);
        ClientSocket client3 = new ClientSocket("localhost", 3333);
        Thread.sleep(500);
        server.killServer();
        Thread.sleep(500);
        assert(!client1.isConnected());
        assert(!client2.isConnected());
        assert(!client3.isConnected());
    }
}