package LoggingSystems;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

class LoggerTest {
    @Test
    @DisplayName("Logger Creation")
    public void testLoggerCreation() throws LoggerNameInUseException {
        Logger.addLogger("ERROR", "", 25);
        assert(Logger.getCurrentLogs().length == 1);
        Logger.killLogger("ERROR");
        assert(Logger.getCurrentLogs().length == 0);
    }

    @Test
    @DisplayName("Logger AddLogs")
    public void testLoggerAddingLogs() throws LoggerNameInUseException, InterruptedException {
        Logger.addLogger("ERROR", "", 25);
        assert(Logger.getCurrentLogs().length > 0);
        Logger.logData("ERROR", "Test1");
        Thread.sleep(15000);
        ArrayList<String> temp = Logger.getLogs("ERROR", 25);
            assert(temp.get(0).contains("Test1"));
        Logger.killLogger("ERROR");
    }

    @Test
    @DisplayName("Logger Excessive Logging")
    public void testLogFalloff() throws LoggerNameInUseException, InterruptedException {
        Logger.addLogger("ERROR", "", 4);
        for(int i = 0; i < 5; i++){
            Logger.logData("ERROR", "Test - " + i);
        }
        Thread.sleep(2000);
        ArrayList<String> returns = Logger.getLogs("ERROR", 4);
        assert(returns.get(0).contains("Test - " + 4));
        Logger.killLogger("ERROR");
    }
}