package RestAPIConnections;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class CoinHistoryTest {
    /*@Test
    @DisplayName("Test Loading Current Value")
    public void loadCurrentValue(){
        CoinHistory ch = new CoinHistory(new String[]{"BTC", "USD"}, new cexIOapi("C:\\Users\\curti\\OneDrive\\Dont Delete Things"), "C:\\Users\\curti\\OneDrive\\Dont Delete Things");
        ch.addCurrentTimeToHistory();
    }

    @Test
    @DisplayName("Test Loading Data From Day")
    public void loadHistoricDay(){
        CoinHistory ch = new CoinHistory(new String[]{"BTC", "USD"}, new cexIOapi("C:\\Users\\curti\\OneDrive\\Dont Delete Things"), "C:\\Users\\curti\\OneDrive\\Dont Delete Things");
        ch.loadHistoryStartingAt(1646481600000L);
    }

    @Test
    @DisplayName("Test Loading for Current Day")
    public void loadHistoryForDay(){
        CoinHistory ch = new CoinHistory(new String[]{"BTC", "USD"}, new cexIOapi("C:\\Users\\curti\\OneDrive\\Dont Delete Things"), "C:\\Users\\curti\\OneDrive\\Dont Delete Things");
        ch.loadHistoryForCurrentDay();
    }

    @Test
    @DisplayName("Test Run System")
    public void testRun(){
        CoinHistory ch = new CoinHistory(new String[]{"BTC", "USD"}, new cexIOapi("C:\\Users\\curti\\OneDrive\\Dont Delete Things"), "C:\\Users\\curti\\OneDrive\\Dont Delete Things");
        ch.run();
    }*/

    @Test
    @DisplayName("test string output")
    public void testString(){
        CoinHistory ch1 = new CoinHistory(new String[]{"BTC", "USD"}, "");
        CoinHistory ch2 = new CoinHistory(new String[]{"LTC", "USD"}, "");
        CoinHistory ch3 = new CoinHistory(new String[]{"ETH", "USD"}, "");
        CoinHistory ch4 = new CoinHistory(new String[]{"DOGE", "USD"}, "");
        CoinHistory ch5 = new CoinHistory(new String[]{"UNI", "USD"},"");

        ch1.startHistoryGathering();
        ch2.startHistoryGathering();
        ch3.startHistoryGathering();
        ch4.startHistoryGathering();
        ch5.startHistoryGathering();

        try {
            Thread.sleep(45*60000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        CoinHistory.saveCurrentHistory();

        File file = new File("C:\\Users\\curti\\Documents\\Hello1.csv");

        // creates the file
        try {
            file.createNewFile();

        // creates a FileWriter Object
        FileWriter writer = new FileWriter(file);

        // Writes the content to the file
        writer.write(CoinHistory.getFormattedHistory(System.currentTimeMillis()/1000L, 60*365*24, 60*24, true));
        writer.flush();
        writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        CoinHistory.getHistoryForCoin("BTC", System.currentTimeMillis()/1000L, 60, 200);
        System.out.println("PRINT OUT DONE");
    }
}