package NeuralNetwork;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class NeuralNetworkNodeTest {

    @Test
    @DisplayName("Creation Testing")
    public void testCreation(){
        NeuralNetworkNode nodeI = new NeuralNetworkNode("0",NeuralNetworkNode.INPUT_NODE);
        NeuralNetworkNode nodeH = new NeuralNetworkNode("1",NeuralNetworkNode.HIDDEN_NODE);
        NeuralNetworkNode nodeO = new NeuralNetworkNode("2",NeuralNetworkNode.OUTPUT_NODE);
        assert(nodeO.getNodeType() == NeuralNetworkNode.OUTPUT_NODE);
        assert(nodeH.getNodeType() == NeuralNetworkNode.HIDDEN_NODE);
        assert(nodeI.getNodeType() == NeuralNetworkNode.INPUT_NODE);
        System.out.println("Created : \n" + nodeI.toString() + "\n" + nodeH.toString() + "\n" + nodeO.toString());
    }

    @Test
    @DisplayName("Adding Connection")
    public void AddConnection(){
        NeuralNetworkNode nodeI = new NeuralNetworkNode("0",NeuralNetworkNode.INPUT_NODE);
        NeuralNetworkNode nodeH = new NeuralNetworkNode("1",NeuralNetworkNode.HIDDEN_NODE);
        NeuralNetworkNode nodeO = new NeuralNetworkNode("2",NeuralNetworkNode.OUTPUT_NODE);

        nodeO.addNodeConnection(nodeH, 0);
        nodeH.addNodeConnection(nodeI, 0);
        nodeO.addNodeConnection(nodeI, 0);

        assert(nodeO.getConnectedNodes().size() == 2);
        assert(nodeI.getConnectedNodes().size() == 0);
        assert(nodeH.getConnectedNodes().size() == 1);

        System.out.println("Created : \n" + nodeI.toString() + "\n" + nodeH.toString() + "\n" + nodeO.toString());
    }

    @Test
    @DisplayName("Base Calculation")
    public void CalculationTest(){
        NeuralNetworkNode nodeI = new NeuralNetworkNode("0",NeuralNetworkNode.INPUT_NODE);
        NeuralNetworkNode nodeH = new NeuralNetworkNode("1",NeuralNetworkNode.HIDDEN_NODE);
        NeuralNetworkNode nodeO = new NeuralNetworkNode("2",NeuralNetworkNode.OUTPUT_NODE);

        nodeH.addNodeConnection(nodeI, .5);
        nodeO.addNodeConnection(nodeH, 2);

        nodeI.setValue(1);
        assert(nodeO.getCalculatedValue() == 1);


        nodeI = new NeuralNetworkNode("0",NeuralNetworkNode.INPUT_NODE);
        nodeH = new NeuralNetworkNode("1",NeuralNetworkNode.HIDDEN_NODE);
        nodeO = new NeuralNetworkNode("2",NeuralNetworkNode.OUTPUT_NODE);

        nodeO.addNodeConnection(nodeH, 2);
        nodeO.addNodeConnection(nodeI, 1);
        nodeH.setValue(1);
        nodeI.setValue(2);
        assert(nodeO.getCalculatedValue() == 4);

        assert(nodeO.getCalculatedValue() == 4);
    }

    @Test
    @DisplayName("Creating Loop")
    public void CreateLoop(){
        NeuralNetworkNode nodeI = new NeuralNetworkNode("0",NeuralNetworkNode.INPUT_NODE);
        NeuralNetworkNode nodeH = new NeuralNetworkNode("1",NeuralNetworkNode.HIDDEN_NODE);
        NeuralNetworkNode nodeO = new NeuralNetworkNode("2",NeuralNetworkNode.OUTPUT_NODE);

        nodeH.addNodeConnection(nodeI, 1);
        nodeO.addNodeConnection(nodeH,1);

        assert(nodeH.getConnectedNodes().contains(nodeI));
        nodeI.addNodeConnection(nodeO, 1);

    }

    @Test
    @DisplayName("Input to Other Link")
    public void InputConnection(){
        NeuralNetworkNode nodeI = new NeuralNetworkNode("0",NeuralNetworkNode.INPUT_NODE);
        NeuralNetworkNode nodeH = new NeuralNetworkNode("1",NeuralNetworkNode.HIDDEN_NODE);
        NeuralNetworkNode nodeO = new NeuralNetworkNode("2",NeuralNetworkNode.OUTPUT_NODE);

        nodeI.addNodeConnection(nodeH, 0);

        nodeI.addNodeConnection(nodeO, 0);
    }

    @Test
    @DisplayName("Remove Edge")
    public void RemoveConnection(){
        NeuralNetworkNode nodeI = new NeuralNetworkNode("0",NeuralNetworkNode.INPUT_NODE);
        NeuralNetworkNode nodeH = new NeuralNetworkNode("1",NeuralNetworkNode.HIDDEN_NODE);
        NeuralNetworkNode nodeO = new NeuralNetworkNode("2",NeuralNetworkNode.OUTPUT_NODE);


    }
}