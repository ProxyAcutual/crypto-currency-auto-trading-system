package NeuralNetwork;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class NeuralNetworkCoreTest {

    @Test
    @DisplayName("Input and Output Creation")
    public void blankCreation(){
        NeuralNetworkCore core = new NeuralNetworkCore(1, 1, new String[]{"TEST"});
        assert(core.getNodesOfType(NeuralNetworkNode.OUTPUT_NODE).size() == 1);
        assert(core.getNodesOfType(NeuralNetworkNode.INPUT_NODE).size() == 1);
        assert(core.getNodesOfType(NeuralNetworkNode.HIDDEN_NODE).size() == 0);
    }

    @Test
    @DisplayName("Input an Output Creation multiName")
    public void SystemCreation(){
        NeuralNetworkCore core = new NeuralNetworkCore(1, 1, new String[]{"TEST", "LAST"});
        assert(core.getNodesOfType(NeuralNetworkNode.OUTPUT_NODE).size() == 2);
        assert(core.getNodesOfType(NeuralNetworkNode.INPUT_NODE).size() == 2);
        assert(core.getNodesOfType(NeuralNetworkNode.HIDDEN_NODE).size() == 0);
        System.out.println(core.toString());
    }

    @Test
    @DisplayName("Recreation From String")
    public void RecreationTest(){
        NeuralNetworkCore core = new NeuralNetworkCore(4, 7, new String[]{"TEST"});
        NeuralNetworkCore core2 = new NeuralNetworkCore(core.toString());
        System.out.println(core.toString());
        System.out.println(core2.toString());
        assert(core.toString().contains(core2.toString()));
    }

    @Test
    @DisplayName("Recreation From String With Connection")
    public void RecreationTestConnection(){
        NeuralNetworkCore core = new NeuralNetworkCore(4, 7, new String[]{"TEST"});
        core.addConnection("TEST-1", "TEST-6", 1);
        NeuralNetworkCore core2 = new NeuralNetworkCore(core.toString());
        assert(core.toString().contains(core2.toString()));
    }
}