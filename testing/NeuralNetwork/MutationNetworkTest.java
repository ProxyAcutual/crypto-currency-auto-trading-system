package NeuralNetwork;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class MutationNetworkTest {

    @Test
    @DisplayName("Base Creation")
    public void baseMutationCreation(){
        MutationNetwork newNetwork = new MutationNetwork(new NeuralNetworkCore(5, 7, new String[]{"TEST"}).toString());
        assert(newNetwork._nodeList.size() == (5+7));
    }

    @Test
    @DisplayName("Test New Random Connection")
    public void ConnectionMutation(){
        for(int i = 0; i < 10; i++) {
            System.out.println("New System==========================");
            MutationNetwork network = new MutationNetwork(new NeuralNetworkCore(5, 5, new String[]{"TEST"}).toString());
            network.setMutationChance(MutationNetwork.NEW_CONNECTION, .75);
            network.setMutationChance(MutationNetwork.NEW_NODE, 0);
            network.setMutationChance(MutationNetwork.EDIT_CONNECTION, 0);
            network.setMutationChance(MutationNetwork.REMOVE_CONNECTION, 0);
            network.setMutationChance(MutationNetwork.REMOVE_NODE, 0);
            network.mutateNetwork();
            System.out.println("Mutation Add Connection \n\n" + network.toString());
        }
    }

    @Test
    @DisplayName("Add Random Node")
    public void NodeMutation(){
        for(int i = 0; i < 10; i++) {
            System.out.println("New System==========================");
            MutationNetwork network = new MutationNetwork(new NeuralNetworkCore(5, 5, new String[]{"TEST"}).toString());
            network.setMutationChance(MutationNetwork.NEW_CONNECTION, 0);
            network.setMutationChance(MutationNetwork.NEW_NODE, 1);
            network.setMutationChance(MutationNetwork.EDIT_CONNECTION, 0);
            network.setMutationChance(MutationNetwork.REMOVE_CONNECTION, 0);
            network.setMutationChance(MutationNetwork.REMOVE_NODE, 0);
            network.mutateNetwork();
            System.out.println("NeuralNetwork.MutationNetwork add node \n\n" + network.toString());
        }
    }

    @Test
    @DisplayName("Test Edit Connection")
    public void EditNodeTest(){
        for(int i = 0; i < 10; i++) {
            System.out.println("New System==========================");
            MutationNetwork network = new MutationNetwork(new NeuralNetworkCore(5, 5, new String[]{"TEST"}).toString());
            network.setMutationChance(MutationNetwork.NEW_CONNECTION, .75);
            network.setMutationChance(MutationNetwork.NEW_NODE, 0);
            network.setMutationChance(MutationNetwork.EDIT_CONNECTION, 0);
            network.setMutationChance(MutationNetwork.REMOVE_CONNECTION, 0);
            network.setMutationChance(MutationNetwork.REMOVE_NODE, 0);
            network.mutateNetwork();
            System.out.println("NeuralNetwork.MutationNetwork add node \n\n" + network.toString());
            network = new MutationNetwork(network.toString());
            network.setMutationChance(MutationNetwork.NEW_CONNECTION, 0);
            network.setMutationChance(MutationNetwork.NEW_NODE, 0);
            network.setMutationChance(MutationNetwork.EDIT_CONNECTION, .75);
            network.setMutationChance(MutationNetwork.REMOVE_CONNECTION, 0);
            network.setMutationChance(MutationNetwork.REMOVE_NODE, 0);
            network.mutateNetwork();
            System.out.println("NeuralNetwork.MutationNetwork add node \n\n" + network.toString());
        }
    }

    @Test
    @DisplayName("Test Remove Connection")
    public void RemoveConnection(){
        for(int i = 0; i < 10; i++) {
            System.out.println("New System==========================");
            MutationNetwork network = new MutationNetwork(new NeuralNetworkCore(5, 7, new String[]{"TEST"}).toString());
            network.setMutationChance(MutationNetwork.NEW_CONNECTION, .75);
            network.setMutationChance(MutationNetwork.NEW_NODE, 0);
            network.setMutationChance(MutationNetwork.EDIT_CONNECTION, 0);
            network.setMutationChance(MutationNetwork.REMOVE_CONNECTION, 0);
            network.setMutationChance(MutationNetwork.REMOVE_NODE, 0);
            network.mutateNetwork();
            System.out.println("NeuralNetwork.MutationNetwork add node \n\n" + network.toString());
            network.setMutationChance(MutationNetwork.NEW_CONNECTION, 0);
            network.setMutationChance(MutationNetwork.NEW_NODE, 0);
            network.setMutationChance(MutationNetwork.EDIT_CONNECTION, 0);
            network.setMutationChance(MutationNetwork.REMOVE_CONNECTION, .75);
            network.setMutationChance(MutationNetwork.REMOVE_NODE, 0);
            network.mutateNetwork();
            System.out.println("NeuralNetwork.MutationNetwork add node \n\n" + network.toString());
        }
    }

    @Test
    @DisplayName("Test Remove Node")
    public void RemoveNode(){
        for(int i = 0; i < 10; i++) {
            System.out.println("New System==========================");
            MutationNetwork network = new MutationNetwork(new NeuralNetworkCore(5, 5, new String[]{"TEST"}).toString());
            network.setMutationChance(MutationNetwork.NEW_CONNECTION, 0);
            network.setMutationChance(MutationNetwork.NEW_NODE, 1);
            network.setMutationChance(MutationNetwork.EDIT_CONNECTION, 0);
            network.setMutationChance(MutationNetwork.REMOVE_CONNECTION, 0);
            network.setMutationChance(MutationNetwork.REMOVE_NODE, 0);
            network.mutateNetwork();
            System.out.println("NeuralNetwork.MutationNetwork add node \n\n" + network.toString());
            network.setMutationChance(MutationNetwork.NEW_CONNECTION, 0);
            network.setMutationChance(MutationNetwork.NEW_NODE, 0);
            network.setMutationChance(MutationNetwork.EDIT_CONNECTION, 0);
            network.setMutationChance(MutationNetwork.REMOVE_CONNECTION, 0);
            network.setMutationChance(MutationNetwork.REMOVE_NODE, .25);
            network.mutateNetwork();
            System.out.println("NeuralNetwork.MutationNetwork add node \n\n" + network.toString());
        }
    }
}