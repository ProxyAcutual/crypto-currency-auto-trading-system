package CryptoControlPackage;

import NeuralNetwork.NeuralNetworkCore;
import RestAPIConnections.CoinHistory;
import RestAPIConnections.cexIOapi;
import RestAPIConnections.cexIOapiCommands;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class CryptoTraderBackend implements Runnable{

    public static final long MILLIS_PER_MINUTE = 60000L;
    public static final int DATA_POINTS_FOR_CALCULATION = 50;

    private static NeuralNetworkCore _coreNetwork;
    private static HashMap<String, Double> _walletValues;
    private static String _historyFileLoc;

    private Thread _thisThread;
    private String[] _monitoredCoins;
    private long _lastUpdatedTimeStamp = 0;
    private int _minutesBetweenChecks;
    private static boolean _activelyTrading = false;
    private boolean _enabled = true;

    public CryptoTraderBackend(String[] monitoredCoins, int minutesBetweenChecks, String historyFile){
        _monitoredCoins = monitoredCoins;
        _walletValues = new HashMap<>();
        _minutesBetweenChecks = minutesBetweenChecks;
        _thisThread = new Thread(this);
        _thisThread.setName("CryptoBackend");
        _historyFileLoc = historyFile;
        _thisThread.start();
    }

    @Override
    public void run(){
        startHistory(_monitoredCoins);

        while(_enabled){
            if(System.currentTimeMillis() > _lastUpdatedTimeStamp){
                _lastUpdatedTimeStamp = System.currentTimeMillis() + (_minutesBetweenChecks * MILLIS_PER_MINUTE);
                ArrayList<String> coins = new ArrayList<>();
                coins.addAll(List.of(_monitoredCoins));

                updateWalletValues();

                actOnHistoricData(coins);
            }
            delay(50);
        }
    }

    public static void setActiveTrading(boolean tradingValue){
        _activelyTrading = tradingValue;
    }

    public static boolean getActiveTrading(){
        return _activelyTrading;
    }

    public static void setNeuralNetwork(NeuralNetworkCore newNetwork){
        _coreNetwork = newNetwork;
    }

    private void actOnHistoricData(ArrayList<String> coins){
        HashMap<String, double[]> historicData = getFormattedHistoricValues(coins);
        HashMap<String, Double> walletValues = getFormattedWallet(coins);

        if(historicData == null){
            System.out.println("NOT ENOUGH DATA");
        }
        else if(_activelyTrading && _coreNetwork != null){
            HashMap<String, double[]> returnVals = _coreNetwork.getResults(historicData, walletValues);
            actOnResults(returnVals);
        }
    }

    private void startHistory(String[] requiredCoins){
        for(String coin: requiredCoins){
            if(!CoinHistory.loadingGivenCoin(coin)){
                CoinHistory newCoin = new CoinHistory(new String[]{coin, "USD"}, _historyFileLoc);
                newCoin.startHistoryGathering();
            }
        }
    }

    private HashMap<String, double[]> getFormattedHistoricValues(ArrayList<String> coins){
        return CoinHistory.getCompressedData(coins, System.currentTimeMillis()/1000L, DATA_POINTS_FOR_CALCULATION, _minutesBetweenChecks, true);
    }

    private HashMap<String, Double> getFormattedWallet(ArrayList<String> coins){
        HashMap<String, Double> retVal = new HashMap<>();
        for(int i = 0; i < coins.size(); i++){
            retVal.put(coins.get(i), _walletValues.get(coins.get(i)));
        }
        retVal.put("USD", _walletValues.get("USD"));
        return retVal;
    }

    private void updateWalletValues(){
        _walletValues = cexIOapiCommands.getWalletValues();
    }

    private void actOnResults(HashMap<String, double[]> networkResults){
        //TODO:Finish this section
    }

    private void delay(int timeMillis){
        try{
            Thread.sleep(timeMillis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
