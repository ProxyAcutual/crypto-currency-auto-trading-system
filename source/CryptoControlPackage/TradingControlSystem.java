package CryptoControlPackage;

import NeuralNetwork.NeuralNetworkCore;
import RestAPIConnections.cexIOapi;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class TradingControlSystem {

    private static final int NUMBER_OF_INPUTS = CryptoTraderBackend.DATA_POINTS_FOR_CALCULATION;
    private static final int NUMBER_OF_OUTPUTS = 1;

    private static String[] _trackedCoins;

    private static NeuralNetworkCore _currentUsedNetwork;

    private String _fileLocation;

    public TradingControlSystem(String fileLoc, int minutesBetween, String[] newCryptos){
        _fileLocation = fileLoc;
        _trackedCoins = newCryptos;
        _currentUsedNetwork = makeNewNeuralNetwork(_fileLocation);
        CryptoTraderBackend cryptoTrader = new CryptoTraderBackend(_trackedCoins, minutesBetween, _fileLocation);
        updateNeuralNetwork(_currentUsedNetwork);
    }

    private NeuralNetworkCore makeNewNeuralNetwork(String fileLoc){
        NeuralNetworkCore retNetwork = null;
        String networkString = readFile(fileLoc + "\\MainControl.NNC");
        if(networkString == ""){
            retNetwork = new NeuralNetworkCore(NUMBER_OF_INPUTS, NUMBER_OF_OUTPUTS, _trackedCoins);
        }
        else {
            retNetwork = new NeuralNetworkCore(networkString);
            if(!neuralNetworkContainsCoins(retNetwork, _trackedCoins)){
                retNetwork = new NeuralNetworkCore(NUMBER_OF_INPUTS, NUMBER_OF_OUTPUTS, _trackedCoins);
            }
        }
        return retNetwork;
    }

    private String readFile(String fileLoc){
        String retVal = "";
        try {
            File file = new File(fileLoc);
            Scanner readers = new Scanner(file);
            while(readers.hasNextLine()){
                retVal = retVal + readers.nextLine() + "\n";
            }
            readers.close();
        } catch (FileNotFoundException e){

        }
        return retVal;
    }

    private void updateNeuralNetwork(NeuralNetworkCore newNet){
        CryptoTraderBackend.setNeuralNetwork(newNet);
        try {
            File file = new File(_fileLocation + "\\MainControl.NNC");
            file.createNewFile();
            FileWriter fw = new FileWriter(file);
            fw.write(newNet.toString());
            fw.flush();
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean neuralNetworkContainsCoins(NeuralNetworkCore _neuralNetwork, String[] coins){
        boolean retVal = true;
        ArrayList<String> watchedCoins = _neuralNetwork.getNeuralNetworkCoin();
        watchedCoins.remove("USD");
        for(String str: coins){
            retVal = retVal && watchedCoins.contains(str);
            watchedCoins.remove(str);
        }
        return retVal && watchedCoins.isEmpty();
    }
}
