package LoggingSystems;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;

/**
 * A logging system so that when where is error logging and temp logs they can be controlled by multiple files.
 * In this system there are some givens there can be no logs of the same name.
 * and there is a maximum log length that can be set.
 *
 * @Author Curtis Andersen
 */
public class Logger implements Runnable{

    //This is a concurrent system to avoid modification errors as this is a static accessed by multiple classes.
    private static ConcurrentHashMap<String, Logger> _loggers = new ConcurrentHashMap<>();

    private int _runningLengthOfLog;
    private String _loggerName;
    private String _logFileLoc;
    private ArrayList<String> _loggerBuffer = new ArrayList<>();
    private Thread _threadLogger;
    private boolean _enabled = true;

    /**
     * Logger creation this needs to be called before the _loggers is accessed as that will remain null until a single
     * logger is created
     * @param loggerName the name of the log to create. This needs to be unique as the if it is not a
     *                   LoggerNameInUseExecution will be called
     * @param defaultFileLocation this is the file location if the default file location is specified
     * @param lengthOfLogs The max length of the log. this is important as if the log is longer than this it will be
     *                     truncated to avoid file size run away.
     */
    public Logger(String loggerName, String defaultFileLocation, int lengthOfLogs){
        _loggerName = loggerName;
        _runningLengthOfLog = lengthOfLogs;
        _logFileLoc = defaultFileLocation;
        _loggerBuffer.add("Logger " + loggerName + " started.");
        _threadLogger = new Thread(this);
        _threadLogger.setName("Logger: " + loggerName);
        _threadLogger.start();
    }

    @Override
    public void run(){
        while(_enabled){
            if(!_loggerBuffer.isEmpty()){
                writeToLog(_loggerBuffer.get(0));
                _loggerBuffer.remove(0);
            }

            try {
                Thread.sleep(25);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Changes the storage length of the given log.
     * @param logger the logger to change the length of
     * @param newLengthOfLogs the new number of lines the log will keep track of
     * @return true if the system is successful in changing the length of the log. False if the log is null or there is
     * some other issue.
     */
    public static boolean changeLogLength(String logger, int newLengthOfLogs){
        Logger tempLogger = _loggers.get(logger);
        if(tempLogger == null){
            return false;
        }
        tempLogger._runningLengthOfLog = newLengthOfLogs;
        return true;
    }

    /**
     * gets a specified number of logs from a given log.
     * @param loggerName the log to grab from
     * @param MaxLengthOfLogs the number of lines to retrieve from the logs
     * @return array list of the lines that were read.
     */
    public static ArrayList<String> getLogs(String loggerName, int MaxLengthOfLogs){
        ArrayList<String> returnValue = new ArrayList<>();
        try{
            Logger logger = _loggers.get(loggerName);

            //This is an if to determine the path with ignoring null pointers.
            Path path = Paths.get(System.getProperty("user.dir"), "Logs", logger._loggerName + ".LOG");

            if(!logger._logFileLoc.matches("")){
                path = Paths.get(logger._logFileLoc, "Logs", logger._loggerName + ".LOG");
            }

            File file = new File(path.toString());

            Scanner reader = new Scanner(file);
            while(reader.hasNextLine() && returnValue.size() < MaxLengthOfLogs){
                returnValue.add(reader.nextLine());
            }
            reader.close();
        }catch(FileNotFoundException e){
            //Do nothing there is a very real possibility this will be hit when the file is first created
        }
        return returnValue;
    }

    /**
     * Gets the current number of lines the log will currently maintain
     * @param logString the logger to look at
     * @return an integer of the number of lines currently watched
     */
    public static int getMaxLogLength(String logString){
        return _loggers.get(logString)._runningLengthOfLog;
    }

    /**
     * This will write a new string from the buffer to the log that it is currently under
     * @param logString the string to write to the log
     */
    private void writeToLog(String logString){
        mkDirectory();
        ArrayList<String> tempLogs = getLogs(_loggerName, _runningLengthOfLog);
        try {
            Path filePath = Paths.get(System.getProperty("user.dir"), "Logs", _loggerName + ".LOG");
            if(!_logFileLoc.matches("")){
                filePath = Paths.get(_logFileLoc, "Logs", _loggerName + ".LOG");
            }

            File file = new File(filePath.toString());
            BufferedWriter bw = new BufferedWriter(new FileWriter(file));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-DD-HH:mm:ss");
            String time = sdf.format(new Date(System.currentTimeMillis()));
            bw.write(time + "\t" + logString + "\n");
            for(String str: tempLogs){
                bw.write(str + "\n");
            }
            bw.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    /**
     * Make a directory for the log if there is not one if there is one do nothing.
     */
    private void mkDirectory(){
        Path filePath = Paths.get(System.getProperty("user.dir"), "Logs");
        if(!_logFileLoc.matches("")){
            filePath = Paths.get(_logFileLoc, "Logs");
        }

        if(!Files.exists(filePath)){
            try {
                Files.createDirectory(filePath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Kills the logger thread with the specified name it will also be removed from the reference hash map
     * @param loggerName the logger to kill
     */
    public static void killLogger(String loggerName){
        _loggers.get(loggerName)._enabled = false;
        _loggers.remove(loggerName);
    }

    /**
     * Add the message to the logger message buffer
     * @param loggerName the name of the logger
     * @param message the message to add
     */
    public static void logData(String loggerName, String message){
        try {
            if(_loggers.contains(loggerName)) {
                _loggers.get(loggerName)._loggerBuffer.add(message);
            }
        }catch (NullPointerException e){
            try {
                Thread.sleep(25);
                logData(loggerName, message);
            }catch (InterruptedException interruptedException) {
                interruptedException.printStackTrace();
            }
        }
    }

    /**
     * gets the current loggers the system is using
     * @return String array of the loggers in use
     */
    public static String[] getCurrentLogs(){
        return _loggers.keySet().toArray(new String[_loggers.keySet().size()]);
    }

    /**
     * Adds a new logger to the logging system
     * @param loggerName the logger Name
     * @param fileLoc the location of the logger if this is left null it will use the current directory
     * @param maxLogFileLength the number of lines tracked by the log
     * @throws LoggerNameInUseException this happens if this name is already in use
     */
    public static void addLogger(String loggerName, String fileLoc, int maxLogFileLength){
        if(_loggers.containsKey(loggerName)) {
            //throw new LoggerNameInUseException("Logger Name " + loggerName + " is in use");
        }

        //This is done in two steps to avoid a null pointer reference as _loggers may be empty and making a new logger
        //will initialize that item
        Logger newLogger = new Logger(loggerName, fileLoc, maxLogFileLength);
        _loggers.put(loggerName, newLogger);
    }

    public static void endAllLoggers(){
        for(String srt: _loggers.keySet()){
            logData(srt, "Killing Log " + srt);
            killLogger(srt);
        }
    }
}
