package LoggingSystems;

import java.text.ParseException;
import java.util.ArrayList;

public class LogConsoleSystem {
    public static final String LOG_KEY = "log";

    public static void LogCommands(ArrayList<String> command){
        String[] commands = command.toArray(new String[command.size()]);
        if(commands.length >= 1){
            if(commands[0].matches("help")){
                help();
            }
            boolean logFound = false;
            for(String log: Logger.getCurrentLogs()){
                logFound = logFound || log.matches(commands[0]);
            }
            if(!logFound){
                System.out.println("NO " + commands[0] + " LOG found");
                commands = new String[0];
            }
        }

        if(commands.length == 0){
            displayCurrentLogs();
        }
        if(commands.length == 1){
            displayLogData(commands[0], Logger.getMaxLogLength(commands[0]));
        }
        for(int i = 1; i < commands.length; i++){
            if(commands[i].matches("--size") && i + 1 < commands.length){
                try {
                    editLogNumber(commands[0], Integer.parseInt(commands[i + 1]));
                }catch(NumberFormatException e){
                    System.out.println("ERROR : " + commands[i + 1] + " is not a valid size");
                }
                i++;
            }
            else if(commands[i].matches("--get")){
                int logLength = Logger.getMaxLogLength(commands[0]);
                if(commands.length > i + 1){
                    try {
                        logLength = Integer.parseInt(commands[i + 1]);
                        i++;
                    }catch (NumberFormatException e){
                        System.out.println("No number given getting full log");
                    }
                }
                displayLogData(commands[0], logLength);
            }
            else{
                help();
            }
        }
    }

    private static void displayLogData(String log, int amountOfLogs){
        if(amountOfLogs == 0){
            amountOfLogs = Logger.getMaxLogLength(log);
        }
        ArrayList<String> data = Logger.getLogs(log, amountOfLogs);
        for(String logs: data){
            System.out.println(logs);
        }
    }

    private static void displayCurrentLogs(){
        String[] currentLogs = Logger.getCurrentLogs();
        System.out.println("\nPlease specify the log to look at. \n\tCurrent Logs are : \n");
        for(String log : currentLogs){
            System.out.println("\t\t- " + log);
        }
        System.out.println("\n");
    }

    private static void editLogNumber(String log, int newNumber){
        if(Logger.changeLogLength(log, newNumber)) {
            System.out.println("Updated Log " + log + " to have a max length of " + newNumber);
            Logger.logData(log, "Updated to contain " + newNumber + " of lines.");
        }
        else{
            System.out.println("Failed to update log");
        }
    }

    private static void help(){
        System.out.println("Usable commands for the log system are as follows");
        System.out.println("Just calling " + LOG_KEY + " will display the current logs");
        System.out.println("\n\t--size\tAllows for the change in the length of the kept logs");
        System.out.println("\t--get\tgets the current logged data for the given log");
        System.out.println("\n");
    }


}
