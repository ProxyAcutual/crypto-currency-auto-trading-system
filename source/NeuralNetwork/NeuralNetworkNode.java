package NeuralNetwork;

import java.util.HashMap;
import java.util.Set;

/**
 * @author Curtis Andersen
 * This is the nodes for the Neural Network
 */
public class NeuralNetworkNode {
    public static final int INPUT_NODE = 0;
    public static final int OUTPUT_NODE = 1;
    public static final int HIDDEN_NODE = 2;


    private final HashMap<NeuralNetworkNode, Double> _nodeConnections = new HashMap<>();

    private final String _id;
    private final int _type;
    private double _value;
    private boolean _calculated = false;


    /**
     * Make a new neural network node
     * @param id this needs a unique id this is how it is recreated
     * @param type the type of node it is
     */
    public NeuralNetworkNode(String id, int type){
        _id = id;
        _type = type;
    }

    /**
     * adds a node to the connection
     * @param newNode the new node to add the connection to
     * @param connectionWeight the weight to put between them
     */
    public void addNodeConnection(NeuralNetworkNode newNode, double connectionWeight){
        if(this._nodeConnections.keySet().contains(newNode) ||
                (getNodeType() != INPUT_NODE && !newNode.connectsTo(this, 0) && newNode != this)) {
            _nodeConnections.put(newNode, connectionWeight);
        }
    }

    /**
     * Remove nodes from the connection system
     * @param node the node to remove from the connection system
     */
    public void removeConnection(NeuralNetworkNode node){
        _nodeConnections.remove(node);
    }

    /**
     * sets the value of this. This is to help cut down on recalculations
     * @param value the value to set the system as
     */
    public void setValue(double value){
        _value = value;
        _calculated = true;
    }

    /**
     * clear the values that were set in the system
     */
    public void clearSetValue(){
        _value = 0;
        _calculated = false;
    }

    /**
     * gets the calculated value for the system one thing to note is that once this is called it can be cut down
     * @return the value that was calculated
     */
    public double getCalculatedValue(){
        if(_calculated) return _value;
        for(NeuralNetworkNode connection: _nodeConnections.keySet()){
            _value += connection.getCalculatedValue() * _nodeConnections.get(connection);
        }
        setValue(_value);
        return _value;
    }

    public double getNodeConnectionWeight(NeuralNetworkNode connectingNode){
        return _nodeConnections.get(connectingNode);
    }

    /**
     * get the unique id of the system
     * @return the id value
     */
    public String getNodeId(){
       return _id;
    }

    /**
     * get the node type
     * @return the node type
     */
    public int getNodeType(){
        return _type;
    }

    /**
     * checks if there is some point up the chain where this is connected to it
     * @param newNode the node that is being looked for
     * @return ture if a loop is created
     */
    public boolean connectsTo(NeuralNetworkNode newNode, int numberDeep) {
        if (_nodeConnections.size() == 0) return false;
        if (_nodeConnections.containsKey(newNode) || newNode == this) return true;
        for (NeuralNetworkNode connectedNode : _nodeConnections.keySet()) {
            if(connectedNode.connectsTo(newNode, numberDeep+1)){
                return true;
            }
        }
        return false;
    }

    public Set<NeuralNetworkNode> getConnectedNodes(){
        return _nodeConnections.keySet();
    }

    /**
     * creates a string that will leave a unique id type and the connections.
     * @return the string of the node
     */
    @Override
    public String toString(){
        String retVal = getNodeId() + ":" + getNodeType() + "|";
        for(NeuralNetworkNode node: _nodeConnections.keySet()){
            retVal = retVal + ":" + node.getNodeId() + "," + _nodeConnections.get(node);
        }
        return retVal;
    }
}
