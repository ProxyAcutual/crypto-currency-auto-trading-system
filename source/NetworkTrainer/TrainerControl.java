package NetworkTrainer;

import LoggingSystems.Logger;
import LoggingSystems.LoggerNameInUseException;
import NeuralNetwork.*;
import RestAPIConnections.CoinHistory;
import RestAPIConnections.cexIOapiCommands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * System that is responsible for making sure that the Neural network mutates and is improved over time.
 */
public class TrainerControl implements Runnable{
    private static final String LOG_NAME = "TrainerControl";

    private ArrayList<IndividualTrainer> _individualTrainer;
    private static boolean _running = true;
    private static boolean _betterNetworkFound = false;
    private static int _maxNumberOfTrainers = 10;

    private static HashMap<String, double[]> _currencyLimits;
    private static ArrayList<String> _targetCoins;

    private static int _timeScaleDelay;

    private static NeuralNetworkCore _currentBestNetwork;
    private static double _currentBestIncrease;


    private static Thread _thisThread;

    public TrainerControl(int deltaTime, String[] targetCoins){
        _timeScaleDelay = deltaTime;
        _targetCoins = new ArrayList<>();
        _targetCoins.addAll(Arrays.stream(targetCoins).toList());
        _individualTrainer = new ArrayList<>();
        _currencyLimits = cexIOapiCommands.getCurrencyLimits();

        Logger.addLogger(LOG_NAME, "",255);

        _thisThread = new Thread(this);
        _thisThread.setName("NetworkTrainerControl");
        _thisThread.start();
    }

    public TrainerControl(int deltaTime, ArrayList<String> targetCoins){
        _timeScaleDelay = deltaTime;
        _targetCoins = targetCoins;
        _individualTrainer = new ArrayList<>();
        _currencyLimits = cexIOapiCommands.getCurrencyLimits();
        Logger.addLogger(LOG_NAME, "",255);
        _thisThread = new Thread(this);
        _thisThread.setName("NetworkTrainerControl");
        _thisThread.start();
    }

    @Override
    public void run(){
        while(_running){
            checkCompletedTrainers();
            addNewTrainers();
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void setNeuralNetwork(MutationNetwork core){
        _currentBestNetwork = core;
    }

    public static void setTargetCoins(ArrayList<String> targetCoins){
        Logger.logData(LOG_NAME, "Updating coins to " + targetCoins.toString());
        _targetCoins = targetCoins;
    }

    public static String getBestNetwork(){
        return _currentBestNetwork.toString();
    }

    /**
     * Ends the training of the system. Should be called on shutdown
     */
    public static void killTraining(){
        Logger.logData(LOG_NAME, "trainers terminated");
        _running = false;
    }

    public static void setHistoricTimeDelta(int newTime){
        Logger.logData(LOG_NAME, "new historic delta set " + newTime);
        _timeScaleDelay = newTime;
    }

    /**
     * Sets the maximum number of trainers for a system. This will directly affect both the efficiency of
     * the training and the amount of computing power the system will use.
     * @param numberOfTrainers
     */
    public static void setNumberOfTrainers(int numberOfTrainers){
        Logger.logData(LOG_NAME, "Updated max number of trainers to " + numberOfTrainers);
        _maxNumberOfTrainers = numberOfTrainers;
    }

    public static int getNumberOfTrainers(){
        return _maxNumberOfTrainers;
    }

    public static boolean hasBetterNetwork(){
        boolean retVal = _betterNetworkFound;
        _betterNetworkFound = false;
        return retVal;
    }

    /**
     * Looks through the array of trainers and when one is found as complete the resulting value is checked
     * to see if the new mutation network has a better mutation.
     */
    private void checkCompletedTrainers(){
        for(int i = 0; i < _individualTrainer.size(); i++){
            if(_individualTrainer.get(i).getIsComplete()){
                if(_individualTrainer.get(i).getNewWalletAmount() > _currentBestIncrease){
                    _currentBestNetwork = _individualTrainer.get(i).getCoreNetwork();
                    _currentBestIncrease = _individualTrainer.get(i).getNewWalletAmount();
                    _betterNetworkFound = true;
                    Logger.logData(LOG_NAME, "Better Network found increase = " + _currentBestIncrease);
                }
                _individualTrainer.remove(i);
                i--;
            }
        }
    }

    /**
     * Populates the active trainers to makes sure that the max number of trainers is obtained
     */
    private void addNewTrainers(){
        while(_individualTrainer.size() < _maxNumberOfTrainers){
            HashMap<String, double[]> history =
                    CoinHistory.getAllHistoryCompressed(_targetCoins, _timeScaleDelay, true);
            if(history != null && _currentBestNetwork != null){
                _individualTrainer.add(new IndividualTrainer
                        (_currentBestNetwork.toString(), true));
            }
        }
    }
}