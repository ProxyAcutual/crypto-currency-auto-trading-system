package NetworkTrainer;

public class CryptoNetworkResult {

    public String buyCoin;
    public String sellCoin;
    public double amount;
    public double[] history;

    public CryptoNetworkResult(String[] coins, double amountInCoin, double[] historicValues){
        buyCoin = coins[1];
        sellCoin = coins[0];
        amount = amountInCoin;
        history = historicValues;
    }

    public double getAmountToSellPerCoin(){
        double retVal = Math.abs(amount) / history[0];
        return retVal;
    }

    public double getAmountToBuyInCoin(){
        double retVal = Math.abs(amount) / history[1];
        return retVal;
    }
}
