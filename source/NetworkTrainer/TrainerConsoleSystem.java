package NetworkTrainer;

import java.util.ArrayList;

public class TrainerConsoleSystem {
    public static final String TRAINER_KEY = "trainer";

    public static int _deltaOptionsTime;
    public static ArrayList<String> _trackedCoins;

    public static void TrainerCommands(ArrayList<String> command){
        String[] commands = command.toArray(new String[command.size()]);
        if(commands.length >= 1){
            if(commands[0].matches("help")){
                help();
            }
            else{
                for(int i = 0; i < commands.length; i++){
                    if(commands[i].matches("--number") && i + 1 < commands.length){
                        try{
                            changeNumberOfTrainers(Integer.parseInt(commands[i + 1]));
                            i++;
                        } catch (NumberFormatException e){
                            System.out.println("Not a valid number for number of trainers");
                        }
                    }
                    else if(commands[i].matches("--delay")){
                        try{
                            changeNumberOfDelay(Integer.parseInt(commands[i + 1]));
                            i++;
                        } catch (NumberFormatException e){
                            System.out.println("Not a valid number for number of trainers");
                        }
                    }
                    else if(commands[i].matches("--start")){
                        startTrainer();
                    }
                    else if(commands[i].matches("--exit")){
                        endTrainers();
                    }
                    else{
                        help();
                    }
                }
            }
        }
        else{
            help();
        }
    }

    public static void endTrainers(){
        TrainerControl.killTraining();
    }

    private static void startTrainer(){
        new TrainerControl(_deltaOptionsTime, _trackedCoins);
        //TODO:: REDO THE TRAINING SYSTEM
    }

    private static void changeNumberOfDelay(int newDelayAmount){
        System.out.println("Changing delay to " + newDelayAmount + " milliseconds");
        //TODO:: MAKE DELAY A CHANGEABLE VALUE
    }

    private static void changeNumberOfTrainers(int numberOfTrainers){
        System.out.println("Changed the number of trainers to " + numberOfTrainers);
        TrainerControl.setNumberOfTrainers(numberOfTrainers);
    }

    private static void help(){
        System.out.println("Usable commands for the training system are as follows");
        System.out.println("Calling " + TRAINER_KEY + " will print out the current system");
        System.out.println("\n\t--number\tSets the number of active trainers");
        System.out.println("\t--delay \tchanges the delays of the trainers");
        System.out.println("\t--start \tstarts the trainer control and if enough history is loaded" +
                " will start sub trainers");
        System.out.println("\t--exit  \tTerminates the trainer control");

    }
}
