package NetworkTrainer;

import NeuralNetwork.NeuralNetworkCore;
import RestAPIConnections.cexIOapiCommands;

import java.util.ArrayList;
import java.util.HashMap;

public class NeuralNetworkCryptoActor {

    private NeuralNetworkCore _coreNetwork;

    public NeuralNetworkCryptoActor(NeuralNetworkCore coreNetwork){
        _coreNetwork = coreNetwork;
    }

    public NeuralNetworkCore getCoreNetwork(){
        return _coreNetwork;
    }

    public CryptoNetworkResult getNetworkResults
            (HashMap<String, double[]> history, HashMap<String, Double> walletValues){
        String[] returnSet = null;
        HashMap<String, double[]> networkResults = _coreNetwork.getResults(history, walletValues);
        for(String[] validPairs: getValidPairs(networkResults, walletValues, history)){
            double newDifference = getHistoricValueDiff(validPairs, history, networkResults);
            if(returnSet == null || newDifference < getHistoricValueDiff(returnSet, history, networkResults)){
                returnSet = validPairs;
            }
        }
        if(returnSet == null){
            return null;
        }
        double[] historySet = new double[2];
        if(returnSet[0].matches(cexIOapiCommands.COMPARISON_CURRENCY)){
            historySet[0] = 1;
        }
        else{
            historySet[0] = history.get(returnSet[0])[0];
        }

        if(returnSet[1].matches(cexIOapiCommands.COMPARISON_CURRENCY)){
            historySet[1] = 1;
        }
        else{
            historySet[1] = history.get(returnSet[1])[0];
        }
        return new CryptoNetworkResult(returnSet, networkResults.get(returnSet[0])[0], historySet);
    }

    private double getHistoricValueDiff(String[] coins, HashMap<String, double[]> history,
                                        HashMap<String, double[]> networkResults){
        double history0 = 1;
        double history1 = 1;
        if(!coins[0].matches(cexIOapiCommands.COMPARISON_CURRENCY)){
            history0 = history.get(coins[0])[0];
        }
        if(!coins[1].matches(cexIOapiCommands.COMPARISON_CURRENCY)){
            history1 = history.get(coins[1])[0];
        }
        return Math.abs((history0 * networkResults.get(coins[0])[0]) + (history1 * networkResults.get(coins[0])[0]));
    }

    private boolean isValidTradeSet(String[] tradeCoins){
        return cexIOapiCommands.isValidPair(tradeCoins[0], tradeCoins[1]);
    }

    private ArrayList<String[]> getValidPairs
            (HashMap<String, double[]> resultValues,
             HashMap<String, Double> walletValues, HashMap<String, double[]> history){

        ArrayList<String[]> returnValue = new ArrayList<>();
        for(String coin1: resultValues.keySet()){
            for(String coin2: resultValues.keySet()){
                String[] coinSet = new String[]{coin1, coin2};
                if(!coin1.matches(coin2) && isValidTrade(coinSet, resultValues, walletValues, history)
                        && isValidTradeSet(coinSet)){
                    returnValue.add(coinSet);
                }
            }
        }
        return returnValue;
    }

    private boolean isValidTrade
            (String[] coins, HashMap<String, double[]> resultValues,
             HashMap<String, Double> walletValues, HashMap<String, double[]> history){
        boolean retVal = (resultValues.get(coins[0])[0] < 0 && resultValues.get(coins[1])[0] < 0);


        if(coins[0].matches("USD")){
            retVal = retVal && lessThanWallet(walletValues.get(coins[0]), resultValues.get(coins[0])[0], 1);
        }
        else {
            retVal = retVal && lessThanWallet
                    (walletValues.get(coins[0]), resultValues.get(coins[0])[0], history.get(coins[0])[0]);
        }
        return retVal;
    }

    private boolean lessThanWallet(double walletValue, double resultAmount, double historicValue){
        double currentWalletUSD = Math.abs(walletValue * historicValue);
        return resultAmount <= currentWalletUSD;
    }
}
