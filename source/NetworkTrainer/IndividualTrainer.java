package NetworkTrainer;

import NeuralNetwork.MutationNetwork;
import NeuralNetwork.NeuralNetworkCore;
import NeuralNetwork.NeuralNetworkNode;
import RestAPIConnections.CoinHistory;
import UserInterface.SettingsLoader;

import java.util.HashMap;

/**
 * The trainer that loops through the history given to it and the neural network and calculates the
 * resulting increase of the entire system
 */
public class IndividualTrainer implements Runnable {
    private final double STARTING_BASE_AMOUNT = 100;
    private static final int MINIMUM_TRADE_AMOUNT = 0;
    private static final int TRANSACTION_PERCENT_COMMISSION = 1;

    private NeuralNetworkCryptoActor _neuralNetworkMath;

    private HashMap<String, Double> _walletValues;

    private Thread _thisTread;

    private boolean _isComplete = false;
    private double _finalWalletValue = 0;

    /**
     * The constructor that runs through the entire system
     *
     * @param networkString the new network string
     * @param mutate        if the network should be mutated
     */
    public IndividualTrainer(String networkString, boolean mutate) {
        MutationNetwork coreNet = new MutationNetwork(networkString);
        if (mutate) {
            coreNet.mutateNetwork();
        }
        _neuralNetworkMath = new NeuralNetworkCryptoActor(coreNet);
        _thisTread = new Thread(this);
        _thisTread.setName("NN_Trainer");
        _thisTread.start();
    }

    /**
     * The main loop of the trainer so that this system can be run without the delay of the entire system
     */
    @Override
    public void run() {
        _finalWalletValue = getNewWalletAmount(getWalletValueResults(makeInitialWalletValues()));
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        _isComplete = true;
    }

    public boolean getIsComplete() {
        return _isComplete;
    }

    public double getNewWalletAmount() {
        return _finalWalletValue;
    }

    public NeuralNetworkCore getCoreNetwork() {
        return _neuralNetworkMath.getCoreNetwork();
    }

    private HashMap<String, Double> makeInitialWalletValues() {
        HashMap<String, Double> retVal = new HashMap<>();
        for (String coin : SettingsLoader.getMonitoredCoins()) {
            retVal.put(coin, 10.0 / CoinHistory.getOldestValue(coin));
        }
        retVal.put("USD", 10.0);
        return retVal;
    }

    private HashMap<String, Double> getWalletValueResults(HashMap<String, Double> initialWalletValues) {
        int endingValue = ((60 / SettingsLoader.getDeltaTimeSetting()) * 365)
                - _neuralNetworkMath.getCoreNetwork().getNumberOfInputs();
        for (int i = 0; i < endingValue; i++) {
            HashMap<String, double[]> subHistory = CoinHistory.getCompressedData
                    (SettingsLoader.getMonitoredCoins(),
                            CoinHistory.getCurrentCorrectedTimestamp() + (i * SettingsLoader.getDeltaTimeSetting() * 60),
                            _neuralNetworkMath.getCoreNetwork().getNumberOfInputs(),
                            SettingsLoader.getDeltaTimeSetting(),
                            false);

            CryptoNetworkResult calcR = _neuralNetworkMath.getNetworkResults(subHistory, initialWalletValues);
            if (calcR != null) {
                initialWalletValues = simulateTrade(initialWalletValues, calcR);
            }
        }
        return initialWalletValues;
    }

    private HashMap<String, Double> simulateTrade(HashMap<String, Double> walletValues, CryptoNetworkResult commands) {
        double coinValue = walletValues.get(commands.sellCoin);
        if(commands.history[0] != 0 && commands.history[1] != 0) {
            coinValue -= commands.getAmountToSellPerCoin();
        }
        walletValues.put(commands.sellCoin, coinValue);
        coinValue = walletValues.get(commands.buyCoin);
        if(commands.history[1] != 0 && commands.history[0] != 0) {
            coinValue += commands.getAmountToBuyInCoin();
        }
        walletValues.put(commands.buyCoin, coinValue);
        return walletValues;
    }

    private double getNewWalletAmount(HashMap<String, Double> walletValues) {
        double amountUSD = 0;
        HashMap<String, double[]> currentHistory = CoinHistory.getCompressedData(
                SettingsLoader.getMonitoredCoins(),
                CoinHistory.getCurrentCorrectedTimestamp(),
                1,
                15,
                true
        );
        for (String coin : walletValues.keySet()) {
            if (coin.matches("USD")) {
                amountUSD += walletValues.get(coin);
            } else {
                amountUSD += currentHistory.get(coin)[0] * walletValues.get(coin);
            }
        }
        return amountUSD;
    }
}