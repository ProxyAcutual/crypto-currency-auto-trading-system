package RestAPIConnections;

import UserInterface.SettingsLoader;

import javax.xml.transform.Result;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;


public class MySQL_Connection {
    private Connection con = null;
    private static final String CRYPTO_HISTORY_TABLE = "historic_values";

    public MySQL_Connection(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(
                    SettingsLoader.getSQL_Data()[0],    //URL
                    SettingsLoader.getSQL_Data()[1],    //Username
                    SettingsLoader.getSQL_Data()[2]);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void postNewHistoricValue(long timeStamp, String crypto, double historicValue){
        String queryStr =
                "INSERT INTO " + CRYPTO_HISTORY_TABLE + " (TimeStamp, " + crypto + ") " +
                        "VALUES (" + timeStamp + "," + historicValue + ")";
        try{
            Statement statement = con.createStatement();
            statement.execute(queryStr);
            statement.close();
        } catch (SQLException throwables) {
           if(throwables.getErrorCode() == 1054){
               addNewColumn(crypto, CRYPTO_HISTORY_TABLE, "decimal(10,4)");
               postNewHistoricValue(timeStamp, crypto, historicValue);
           }
           else if(throwables.getErrorCode() == 1062){
                updateExistingTimestamp(crypto, CRYPTO_HISTORY_TABLE, historicValue, timeStamp);
           }
           else{
               throwables.printStackTrace();
           }
        }
    }

    public void closeConnection(){
        try {
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void addNewColumn(String columnName, String table, String dataType){
        String queryStr =
                "ALTER TABLE " + table +
                " ADD COLUMN " + columnName + " " + dataType;

        try{
            Statement statement = con.createStatement();
            statement.execute(queryStr);
            statement.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private void updateExistingTimestamp(String columnName, String table, double value, long timeStamp){
        String queryStr =
                "UPDATE " + table + " " +
                "SET " + columnName + " = " + value + " " +
                "WHERE TimeStamp = " + timeStamp;
        try{
            Statement statement = con.createStatement();
            statement.execute(queryStr);
            statement.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public ArrayList<Long> getCurrentlyPopulatedTimeStamps(String coin, long endingTimeStamp){
        ArrayList<Long> retVal = new ArrayList<>();
        String queryStr =
                "SELECT TimeStamp " +
                "FROM " + CRYPTO_HISTORY_TABLE + " " +
                "WHERE " + coin + " IS NOT null and TimeStamp >= " + endingTimeStamp + " " +
                "LIMIT 0, 300000";
        try {
            Statement statement = con.createStatement();
            ResultSet rs = statement.executeQuery(queryStr);
            while(rs.next()){
                retVal.add(rs.getLong("TimeStamp"));
            }
            statement.close();
            rs.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return retVal;
    }

    public HashMap<Long, Double> getHistoricValuesForCoin(String coin, long startingTimeStamp, long endingTimeStamp){
        HashMap<Long, Double> retValue = new HashMap<>();
        String queryStr =
                "SELECT TimeStamp, " + coin + " " +
                "FROM " + CRYPTO_HISTORY_TABLE + " " +
                "WHERE " + coin + " IS NOT null AND " +
                        "TimeStamp >= " + endingTimeStamp + " AND TimeStamp <= " + startingTimeStamp + " " +
                "LIMIT 0, 300000";
        try{
            Statement statement = con.createStatement();
            ResultSet rs = statement.executeQuery(queryStr);
            while(rs.next()){
                retValue.put(rs.getLong("TimeStamp"), rs.getDouble(coin));
            }
            statement.close();
            rs.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return retValue;
    }

    public HashMap<Long, double[]> getHistoricValuesForAll(String[] coins, long startingTime, long endingTime){
        HashMap<Long, double[]> retValue = new HashMap<>();
        String queryStr =
                "SELECT * " +
                "FROM " + CRYPTO_HISTORY_TABLE + " " +
                "WHERE TimeStamp >= " + endingTime + "AND TimeStamp <= " + startingTime + " " +
                "LIMIT 0, 300000";
        try{
            Statement statement = con.createStatement();
            ResultSet rs = statement.executeQuery(queryStr);
            while(rs.next()){
                double[] values = new double[coins.length];
                for(int i = 0 ; i < coins.length; i ++){
                    values[i] = rs.getDouble(coins[i]);
                }
                retValue.put(rs.getLong("TimeStamp"), values);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return retValue;
    }
}
