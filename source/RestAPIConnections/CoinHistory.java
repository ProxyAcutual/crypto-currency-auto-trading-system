package RestAPIConnections;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author Curtis Andersen
 * This is a class to load the history of a given coin using CEX_IO.
 */
public class CoinHistory implements Runnable{
    private static final long FREQUENCY_OF_UPDATES = 60000;
    private static final double MAX_NUMBER_OF_YEARS_BACK = 1;
    private static final long LENGTH_OF_DAY = 60*60*24 - 60*60;
    private static final int DAYS_FOR_DATA_LOAD = 2;

    private String[] _coinPair;
    private Thread _thisTread;
    private ArrayList<Long> _missingTimeStamps = new ArrayList<>();
    private static long _oldestTimeStamp = 0;
    private static cexIOapi _cexIO;
    private static ConcurrentHashMap<Long, ArrayList<Double>> _history = new ConcurrentHashMap<>();
    private static ArrayList<String> _cryptoCheatSheet = new ArrayList<>();
    private static String _fileSavingLocation;
    private static MySQL_Connection sqlConn = new MySQL_Connection();

    private long _lastUpdateTime = -1;

    private boolean _enabled = true;
    private boolean _dayCheckFlop = false;

    private int _cheatSheetPosition;

    /**
     * Creates a new instance of this monitoring system for a given coin.
     * @param coinPair the coins to check between this should follow the {"BTC","USD"}
     */
    public CoinHistory(String[] coinPair, String fileLoc){
        _coinPair = coinPair;
        _cheatSheetPosition = _cryptoCheatSheet.size();
        //Update the new coin to the cheat sheet for quick crypto look up
        _cryptoCheatSheet.add(coinPair[0]);
        _fileSavingLocation = fileLoc;
    }

    /**
     * Starts the independent thread for gathering the data remotely.
     */
    public void startHistoryGathering(){
        _thisTread = new Thread(this);
        _thisTread.setName("HistoryTracker:" + _coinPair[0] + "," + _coinPair[1]);
        _thisTread.start();
    }

    /**
     * This will kill the gathering system. But will not remove the coin from the list of coins
     */
    public void killHistoryGathering(){
        _thisTread.interrupt();
    }

    /**
     * The starting of the thread.
     */
    @Override
    public void run(){
        //Set the last updated to the current system time.
        _lastUpdateTime = FREQUENCY_OF_UPDATES * (System.currentTimeMillis()/FREQUENCY_OF_UPDATES);
        //Gather all the missing time stamps.
        _missingTimeStamps = getMissingTimeStamps();
        //While the system is enabled keep updating the current price of the coin.
        while(_enabled){
            //If the time is pass the last updating system then grab the new value for the current time
            if(System.currentTimeMillis() > _lastUpdateTime){
                _lastUpdateTime += FREQUENCY_OF_UPDATES;
                _dayCheckFlop = true;

                addCurrentTimeToHistory();
            }
            //If there is no missing time stamps just move past
            if(_missingTimeStamps.size() != 0) {
                loadHistoryBefore();
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Get the historic values for a given coin it should be known that start time should be timestamp needs to be
     * in second notation not in the millis time
     * @param coin the name of the coin to get the history for
     * @param startingTimestamp the timestamp of the starting history
     * @param delayBetween the delay between the elements in the data
     * @param numberOfElements the number of data elements to get
     * @return return a double array of elements to be split by the delayBetween
     */
    public static double[] getHistoryForCoin
            (String coin, long startingTimestamp, int delayBetween, int numberOfElements){
        double[] returnValue = new double[numberOfElements];
        //This will make sure the startingTimestamp is in minute time notation.
        startingTimestamp = FREQUENCY_OF_UPDATES * (startingTimestamp/FREQUENCY_OF_UPDATES);
        boolean isFullData = true;
        for(int i = 0; i < returnValue.length; i++){
            returnValue[i] = getGetClosestValue(coin, startingTimestamp -
                    (i * delayBetween * (FREQUENCY_OF_UPDATES / 1000L)), delayBetween);
            //check if the return value is 0
            isFullData = isFullData && returnValue[i] != 0;
        }
        return returnValue;
    }

    public static long getCurrentCorrectedTimestamp(){
        return FREQUENCY_OF_UPDATES * (System.currentTimeMillis()/FREQUENCY_OF_UPDATES) / 1000L;
    }

    /**
     * generated a two-dimensional array of the values it should be known that this gives the values that is closest
     * to the system.
     * @param cryptoMaps the map of the crypto values to generate.
     * @param startingTime the time of the starting the data system needs to be in second notation
     * @param numberOfElements the number of elements to generate for the system
     * @param minutesApart the amount of minutes between each of the values in each of the doubles
     * @param insureIntegrity insures that in the return there are no 0's present
     * @return the two-dimensional array of the values for the system.
     */
    public static HashMap<String, double[]> getCompressedData
            (ArrayList<String> cryptoMaps, long startingTime, int numberOfElements, int minutesApart,
             boolean insureIntegrity){
        //generate the double array of numberOfElements and then the crypto Items
        HashMap<String, double[]> retVal = new HashMap<>();
        boolean zeroFound = false;
        for(int currentCrypto = 0; currentCrypto < cryptoMaps.size(); currentCrypto++){
            //generate the history for the given coin using all the items
            double[] coinHistory = getHistoryForCoin
                    (cryptoMaps.get(currentCrypto), startingTime, minutesApart, numberOfElements);
            retVal.put(cryptoMaps.get(currentCrypto), coinHistory);
        }

        //if there is no full integrity of the return, return null;
        boolean allCoinsZero = true;
        if(insureIntegrity){
            boolean allCoinsAreFull = true;
            for(int i = 0; i < cryptoMaps.size(); i++){
                boolean nonZeroFound = true;
                boolean zeroElementFound = true;
                for(int j = 0; j < numberOfElements; j++){
                    nonZeroFound = nonZeroFound && retVal.get(cryptoMaps.get(i))[j] != 0;
                    zeroElementFound = zeroElementFound && retVal.get(cryptoMaps.get(i))[j] == 0;
                }
                allCoinsZero = allCoinsZero && !nonZeroFound;
                allCoinsAreFull = allCoinsAreFull && !(nonZeroFound && zeroElementFound);
            }

            if(!allCoinsAreFull || allCoinsZero) {
                retVal = null;
            }
        }

        return retVal;
    }

    public static HashMap<String, double[]> getCompressedData
            (String[] cryptoMaps, long startingTime, int numberOfElements, int minutesApart,
             boolean insureIntegrity){
        ArrayList<String> coins = new ArrayList<>();
        coins.addAll(Arrays.stream(cryptoMaps).toList());
        return getCompressedData(coins, startingTime, numberOfElements, minutesApart, insureIntegrity);
    }

    public static HashMap<String, double[]> getAllHistoryCompressed(
            ArrayList<String> coins, int deltaTimeForHistory, boolean allowZeros){
        HashMap<String, double[]> returnVal = new HashMap<>();
        long currentTimeStamp = (FREQUENCY_OF_UPDATES * (System.currentTimeMillis()/FREQUENCY_OF_UPDATES))/1000L;
        int numberOfElements = (int)((currentTimeStamp - _oldestTimeStamp) / deltaTimeForHistory);
        for(String str: coins){
            returnVal.put(str, getHistoryForCoin(str, currentTimeStamp, deltaTimeForHistory, numberOfElements));
        }

        if(!allowZeros){
            for(String str: coins){
                if(returnVal != null) {
                    boolean zeroFound = false;
                    for (double value : returnVal.get(str)) {
                        if(zeroFound && value != 0){
                            returnVal = null;
                        }
                        if(value == 0){
                            zeroFound = true;
                        }
                    }
                }
            }
        }
        return returnVal;
    }

    /**
     * Gets the coins in the system
     * @return the coins of that are being monitored by the system
     */
    public static ArrayList<String> getMonitoredCoins(){
        return _cryptoCheatSheet;
    }

    /**
     * Makes a formatted string using all the systems
     * @param startingTime the time to start all the elements
     * @param numberOfMinutesBack the number of minutes back in time
     * @param minutesBetween the minutes between the elements in the system
     * @param cleanHistory if this is false if there is a 0 element then there will be no editing of the data
     * @return the string of all the data this is good to be shoved in to a csv
     */
    public static String getFormattedHistory
        (long startingTime, int numberOfMinutesBack, int minutesBetween, boolean cleanHistory){
        String retVal = "TIMESTAMP,";
        //add the coins to the start of the string
        for(String coin :  _cryptoCheatSheet){
            retVal = retVal + coin + ",";
        }
        retVal = retVal + "\n";
        startingTime = (FREQUENCY_OF_UPDATES/1000L) * (startingTime/(FREQUENCY_OF_UPDATES/1000L));

        //get all the data for the history needed
        HashMap<String, double[]> cryptoClosestValues =
                getCompressedData(_cryptoCheatSheet, startingTime,
                        numberOfMinutesBack/minutesBetween, minutesBetween, false);

        //loop through all the elements properly putting the items in the string
        for(int i = 0; i < cryptoClosestValues.get(_cryptoCheatSheet.get(0)).length; i++){
            retVal = retVal + ((startingTime - (minutesBetween * i * FREQUENCY_OF_UPDATES)/1000L)) + ",";
            for (String coin: cryptoClosestValues.keySet()) {
                if(cleanHistory && cryptoClosestValues.get(coin)[i] == 0){
                    if(i > 0){
                        cryptoClosestValues.get(coin)[i] = cryptoClosestValues.get(coin)[i-1];
                    }
                }
                retVal = retVal + cryptoClosestValues.get(coin)[i] + ",";
            }
            retVal = retVal + "\n";
        }
        return retVal;
    }

    /**
     * This will return if that coin already has a loaded history.
     * @param coinInQuestion the crypto to check
     * @return if the coin is currently being used this will return true
     */
    public static boolean loadingGivenCoin(String coinInQuestion){
        return _cryptoCheatSheet.contains(coinInQuestion);
    }

    /**
     * Saves the current History to a file location.
     */
    public static void saveCurrentHistory(){
        try{
            File file = new File(_fileSavingLocation + "\\CryptoTrading.HST");
            file.createNewFile();
            FileWriter fw = new FileWriter(file);
            String newLine = "TimeStamp";
            for (int i = 0; i < _cryptoCheatSheet.size(); i++){
                newLine = newLine + "," + _cryptoCheatSheet.get(i);
            }
            fw.write(newLine + "\n");
            for(long timeStamp: _history.keySet()){
                newLine = "" + timeStamp;
                for(int i = 0; i < _cryptoCheatSheet.size(); i++){
                    if(i < _history.get(timeStamp).size()){
                        newLine = newLine + "," +_history.get(timeStamp).get(i);
                    }
                    else{
                        newLine = newLine + "," + 0;
                    }
                }
                fw.write(newLine + "\n");
            }
            fw.flush();
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static double getOldestValue(String coin){
        long oldestValue = Long.MAX_VALUE;
        int coinLoc = _cryptoCheatSheet.indexOf(coin);
        for(long historicVal : _history.keySet()){
            if(_history.get(historicVal).size() > coinLoc && _history.get(historicVal).get(coinLoc) != 0 && historicVal < oldestValue){
                oldestValue = historicVal;
            }
        }
        return _history.get(oldestValue).get(coinLoc);
    }

    /**
     * add the current coin value to the history tracker
     */
    public void addCurrentTimeToHistory(){
        String data =
                _cexIO.getWepPage("https://cex.io/api/last_price/" + _coinPair[0] + "/" + _coinPair[1], "");
        String[] dataArray = data.split(":\"");
        dataArray = dataArray[1].split("\",");
                                                                                                                            System.out.println(_coinPair[0] + " , " +  convertUnixToRealWithSeconds(_missingTimeStamps.get(_missingTimeStamps.size() - 1)) + ", " + dataArray[0]);
        putTimestamp(_lastUpdateTime/1000L, Double.parseDouble(dataArray[0]));
    }

    /**
     * Loads the history before the current day
     */
    public void loadHistoryBefore(){
        //check if the current history for yesterday has any changes
        if(_dayCheckFlop){
            loadHistoryForCurrentDay();
            _dayCheckFlop = false;
        }
        else{
            //load the history for some given days
            if(!loadHistoryStartingAt(_missingTimeStamps.get(_missingTimeStamps.size() - 1)) && isOverDayTimeThreshHold
                    (System.currentTimeMillis()/1000L, _missingTimeStamps.get(_missingTimeStamps.size() - 1))){
                String currentDate = convertUnixToGMT(_missingTimeStamps.get(_missingTimeStamps.size() - 1));
                while(convertUnixToGMT(_missingTimeStamps.get(_missingTimeStamps.size() - 1)).matches(currentDate)) {
                    _missingTimeStamps.remove(_missingTimeStamps.size() - 1);
                }
            }
        }
    }

    /**
     * Loads the history for the current day. this only does 120 elements so,
     * it needs to be called repeatedly as this will allow for the needed updates
     */
    public void loadHistoryForCurrentDay(){
        String body = "{\n" +
                "  \"lastHours\": " + 24 + ",\n" +
                "  \"maxRespArrSize\": "+ 120 + "\n" +
                "}";
        String data = _cexIO.getWepPage
                ("https://cex.io/api/price_stats/" + _coinPair[0] + "/" + _coinPair[1], body);
        String[] brokenData = data.split("},\\{");
        HashMap<Long, Double> gatheredVals = new HashMap<>();
        for(String timeData : brokenData){
            timeData = timeData.replace("[{", "").replace("}]", "");
            String[] values = timeData.replace("\"", "").replace("tmsp:","")
                    .replace("price:","").split(",");
            gatheredVals.put(Long.parseLong(values[0]), Double.parseDouble(values[1]));
        }
        ArrayList<Long> missingValues = getMissingTimeStamps();
        for(Long timeStamp : gatheredVals.keySet()){
            if(missingValues.contains(timeStamp)){
                putTimestamp(timeStamp, gatheredVals.get(timeStamp));
                missingValues.remove(timeStamp);
            }
        }
    }

    /**
     * Load the history started at a current time stamp. it should be known that if there is no data the system will
     * return false
     * @param dayUnixCode the day to load
     * @return true if there were values on that day that needed to be loaded and false if there was no data loaded
     */
    public boolean loadHistoryStartingAt(long dayUnixCode){
        String webLink = "https://cex.io/api/ohlcv/hd/" + convertUnixToGMT(dayUnixCode)
                + "/" + _coinPair[0] + "/" + _coinPair[1];
        String data = _cexIO.getWepPage( webLink, "");
        boolean oneReplaced = false;
        if(data.matches("null") || data.contains("\"data1m\":\"[]\",\"data1h\":\"[")){
            return false;
        }
        String[] dataString = data.split("data1m\":\"\\[\\[")[1].split("]]")[0].split("],\\[");
        for(int i = 0; i < dataString.length; i++){
            String[] timeData = dataString[i].split(",");
            long timeStamp = Long.parseLong(timeData[0]);
            if(timeStamp >= dayUnixCode && _missingTimeStamps.contains(timeStamp)) {
                putTimestamp(timeStamp, Double.parseDouble(timeData[1]));
                _missingTimeStamps.remove(timeStamp);
                oneReplaced = true;
            }
            else if(isOverDayTimeThreshHold(_lastUpdateTime/1000L, timeStamp)){
                _missingTimeStamps.remove(timeStamp);
            }
            if(System.currentTimeMillis() > _lastUpdateTime){
                //Added in as a check to make sure the system doesn't time out. And will add all the needed updates
                return true;
            }
        }
        int i = _missingTimeStamps.size() - 1;
        while(_missingTimeStamps.get(i) < Long.parseLong(dataString[dataString.length - 1].split(",")[0])){
            _missingTimeStamps.remove(i);
            i--;
        }
        return oneReplaced;
    }


    /**
     * Converts the unix time to date
     * @param unixCode the unix time code this should be in seconds
     * @return the date in YYYYMMDDHHSS
     */
    private String convertUnixToRealWithSeconds(long unixCode){
        Date date = new Date(unixCode*1000L);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd:HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        return sdf.format(date);
    }

    /**
     * Converts the unix time to date
     * @param unixCode the unix time code this should be in seconds
     * @return the date in YYYYMMDD
     */
    private String convertUnixToGMT(long unixCode){
        Date date = new Date(unixCode*1000L);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        return sdf.format(date);
    }

    /**
     * Check if the unix time codes are a specific number of elements apart
     * @param unixCode1 the time code 1 should be in second notation
     * @param unixCode2 the time code 2 should be in second notation
     * @return true or false depending on the number of days to load.
     */
    private boolean isOverDayTimeThreshHold(long unixCode1, long unixCode2){
        return Math.abs(unixCode1 - unixCode2) > DAYS_FOR_DATA_LOAD*LENGTH_OF_DAY;
    }

    /**
     * generate a list of all the missing timestamps in the day.
     * @return return the list of elements for the missing items
     */
    private ArrayList<Long> getMissingTimeStamps(){
        ArrayList<Long> retVal = new ArrayList<>();
        ArrayList<Long> currentTimeStamp = sqlConn.getCurrentlyPopulatedTimeStamps
                (_coinPair[0], (long)((_lastUpdateTime/1000L) - (60.0 * 60.0 * 24.0 * 365.0 * MAX_NUMBER_OF_YEARS_BACK)));
        //loop through all the possible timestamps for the maximum number of years in the past
        for(int i = 0; i < (int)(60.0*24.0*365.0*MAX_NUMBER_OF_YEARS_BACK); i++){
            //(_lastUpdateTime-(i*(FREQUENCY_OF_UPDATES)))/1000L);
            if(currentTimeStamp.contains((_lastUpdateTime-(i*FREQUENCY_OF_UPDATES))/1000L)){
                currentTimeStamp.remove((_lastUpdateTime-(i*FREQUENCY_OF_UPDATES))/1000L);
            }
            else {
                retVal.add((_lastUpdateTime - (i * FREQUENCY_OF_UPDATES)) / 1000L);
            }
        }
        return retVal;
    }

    /**
     * Adds a timestamp to the static data system
     * @param timestamp the timestamp to add to the system
     * @param cryptoValue the crypto value to add at the time stamp
     */
    private void putTimestamp(long timestamp, double cryptoValue){
        ArrayList<Double> valuesArray;
        //if the value already exists add it to the system
        if(_history.containsKey(timestamp)){
            valuesArray = _history.get(timestamp);
        }
        //otherwise make a new array to populated with data
        else{
            valuesArray = new ArrayList<>();
            if(timestamp < _oldestTimeStamp || _oldestTimeStamp == 0){
                _oldestTimeStamp = timestamp;
            }
        }
        //fill the array up to the needed size
        for(int i = valuesArray.size(); i < _cheatSheetPosition+1; i++){
            valuesArray.add(valuesArray.size(), 0.0);
        }
        //set the value needed
        valuesArray.set(_cheatSheetPosition, cryptoValue);
        //put the crypto value.
        _history.put(timestamp, valuesArray);

        sqlConn.postNewHistoricValue(timestamp, _coinPair[0], cryptoValue);
    }

    /**
     * Gets the closest documented value to the timestamp. if there is not one within the minimumCount/2 in either
     * direction then this will return 0.
     * @param coin the coin to get the information for
     * @param timeStamp this is the time stamp that the user wants to find the closest known value to. It is important
     *                  that the timestamp given is a value of 60 as the neural network only supports vales on minute
     *                  increments.
     * @param minimumCount this is the amount of minutes that the system will be considered valid in.
     * @return this will return the closest value that the system found if there is no close value found then a 0 will
     *          be returned.
     */
    private static double getGetClosestValue(String coin, long timeStamp, int minimumCount){
        //log the index location of the coin being looked for
        int coinLoc = _cryptoCheatSheet.indexOf(coin);
        double retVal = 0;
        int currentLoc = 0;
        //loops though the known values around the time stamp checking on if they have a valid number
        while(retVal == 0 && currentLoc <= (minimumCount*2)/3){
            //check to see if there is a value above and below the given timestamp a given number of minutes away.
            ArrayList<Double> valuesAbove = _history.get(timeStamp + (currentLoc * (FREQUENCY_OF_UPDATES / 1000L)));
            ArrayList<Double> valuesBelow = _history.get(timeStamp - (currentLoc * (FREQUENCY_OF_UPDATES / 1000L)));
            currentLoc++;

            //makes sure that the system has an array list and the size is large enough if there is one.
            if(valuesBelow != null && valuesBelow.size() > coinLoc){
                retVal = valuesBelow.get(coinLoc);
            }
            if(valuesAbove != null && valuesAbove.size() > coinLoc){
                retVal = valuesAbove.get(coinLoc);
            }
        }
        return retVal;
    }

}
