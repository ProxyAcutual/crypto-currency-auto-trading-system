package ClientServer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

/**
 * The server side of a client socket connection. This will close down socket connections if it finds that
 * they have been disconnected. It will also allow for unhindered reading and writing to the socket
 */
public class ServerControl implements Runnable{
    private ArrayList<ClientSocket> _connectedSockets;
    private ServerSocket _server;
    private Thread _thisThread;
    private boolean _running = true;

    /**
     * This is the creation of the server and the core running systems needed for the server side of the socket
     * @param port this is the port the sockets will connect to
     */
    public ServerControl(int port){
        try {
            _server = new ServerSocket(port);
        } catch (IOException e) {
            e.printStackTrace();
        }
        _connectedSockets = new ArrayList<>();
        _thisThread = new Thread(this);
        _thisThread.start();
    }

    /**
     * Allows for the constant running loop and does not hinder the adding and deleting of connected sockets
     */
    @Override
    public void run() {
        while(_running){
            try {
                Thread.sleep(250);
                Socket newConnection = _server.accept();
                _connectedSockets.add(new ClientSocket(newConnection));
            } catch (IOException | InterruptedException e) {
                killServer();
            }
        }
    }

    /**
     * This will kill the server as well as disconnecting the client sockets
     */
    public void killServer(){
        _running = false;
        _thisThread.interrupt();
        for(ClientSocket socket: _connectedSockets){
            socket.killSocket();
        }
        try {
            _server.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This will get the connected sockets to the server allowing for targeted communication
     * @return the array of connected sockets. Editing this array will not change the internal structure
     */
    public ClientSocket[] getConnections(){
        removeDeadConnections();
        return _connectedSockets.toArray(new ClientSocket[_connectedSockets.size()]);
    }

    /**
     * removes the connections that have been terminated on the other end of the socket.
     */
    private void removeDeadConnections(){
        for(int i = 0; i < _connectedSockets.size(); i++){
            if(!_connectedSockets.get(i).isConnected()){
                _connectedSockets.remove(i);
                i--;
            }
        }
    }
}
